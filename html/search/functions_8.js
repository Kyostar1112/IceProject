var searchData=
[
  ['intersect',['Intersect',['../class_delaunay_1_1_vertex.html#a60b50698d789941e3d233149351d6ea0',1,'Delaunay::Vertex']]],
  ['intpoint',['IntPoint',['../struct_clipper_lib_1_1_int_point.html#a8c9227876a68a75ebf27c80ee15bcdc7',1,'ClipperLib.IntPoint.IntPoint(cInt X, cInt Y)'],['../struct_clipper_lib_1_1_int_point.html#a7a86b4d1c9e3bffc1fb8b1d9b19cee6b',1,'ClipperLib.IntPoint.IntPoint(double x, double y)'],['../struct_clipper_lib_1_1_int_point.html#af0cc1fba195403701dc7a7335ee8138d',1,'ClipperLib.IntPoint.IntPoint(IntPoint pt)']]],
  ['intrect',['IntRect',['../struct_clipper_lib_1_1_int_rect.html#a600f3598825c1dd7f64debb4b49674ba',1,'ClipperLib.IntRect.IntRect(cInt l, cInt t, cInt r, cInt b)'],['../struct_clipper_lib_1_1_int_rect.html#a88b12838e97f663c33753c828730fc7c',1,'ClipperLib.IntRect.IntRect(IntRect ir)']]],
  ['ispartofconvexhull',['IsPartOfConvexHull',['../class_delaunay_1_1_edge.html#a013ca8f4308240102ee0b04eae40c08d',1,'Delaunay::Edge']]]
];
