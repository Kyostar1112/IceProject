var indexSectionsWithContent =
{
  0: "_abcdefghijklmnoprstuvwxy",
  1: "bcdefghijlmnprstuvw",
  2: "cdu",
  3: "bcdefghijlprstuvw",
  4: "abcdefghijlmnoprstuvw",
  5: "_abcdefhijlmnoprstuvwxy",
  6: "cp",
  7: "bcdefjkpsvw",
  8: "abcdeghijlmnoprstv",
  9: "aceilmprstvxy",
  10: "du"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "properties",
  10: "defines"
};

var indexSectionLabels =
{
  0: "全て",
  1: "クラス",
  2: "名前空間",
  3: "ファイル",
  4: "関数",
  5: "変数",
  6: "型定義",
  7: "列挙型",
  8: "列挙値",
  9: "プロパティ",
  10: "マクロ定義"
};

