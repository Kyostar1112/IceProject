﻿using UnityEngine;

public class SceneLoader : MonoBehaviour
{
    private void OnGUI()
    {
        GUI.Box(new Rect(10, Screen.height - 100, 100, 90), "Change Scene");
        if (GUI.Button(new Rect(20, Screen.height - 70, 80, 20), "Next"))
            LoadNextScene();
        if (GUI.Button(new Rect(20, Screen.height - 40, 80, 20), "Back"))
            LoadPreScene();
    }

    private void LoadPreScene()
    {
#pragma warning disable CS0618 // 型またはメンバーが古い形式です
        int nextLevel = Application.loadedLevel + 1;
#pragma warning restore CS0618 // 型またはメンバーが古い形式です
        if (nextLevel <= 1)
#pragma warning disable CS0618 // 型またはメンバーが古い形式です
            nextLevel = Application.levelCount;
#pragma warning restore CS0618 // 型またはメンバーが古い形式です

#pragma warning disable CS0618 // 型またはメンバーが古い形式です
        Application.LoadLevel(nextLevel);
#pragma warning restore CS0618 // 型またはメンバーが古い形式です
    }

    private void LoadNextScene()
    {
#pragma warning disable CS0618 // 型またはメンバーが古い形式です
        int nextLevel = Application.loadedLevel + 1;
#pragma warning restore CS0618 // 型またはメンバーが古い形式です
#pragma warning disable CS0618 // 型またはメンバーが古い形式です
        if (nextLevel >= Application.levelCount)
#pragma warning restore CS0618 // 型またはメンバーが古い形式です
            nextLevel = 1;

#pragma warning disable CS0618 // 型またはメンバーが古い形式です
        Application.LoadLevel(nextLevel);
#pragma warning restore CS0618 // 型またはメンバーが古い形式です
    }
}