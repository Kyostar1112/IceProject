﻿using UnityEngine;

public class BreakUi : MonoBehaviour
{
    private Explodable _explodable;

    // Use this for initialization
    private void Start()
    {
        _explodable = GetComponent<Explodable>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKey(KeyCode.H))
        {
            _explodable.explode();
            ExplosionForce ef = GameObject.FindObjectOfType<ExplosionForce>();
            ef.doExplosion(transform.position);
        }
    }
}