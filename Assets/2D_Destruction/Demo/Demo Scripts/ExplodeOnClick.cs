﻿using UnityEngine;

[RequireComponent(typeof(Explodable))]
public class ExplodeOnClick : MonoBehaviour
{
    private Explodable _explodable;

    private void Start()
    {
        _explodable = GetComponent<Explodable>();
    }

    private void OnMouseDown()
    {
        _explodable.explode();
        ExplosionForce ef = GameObject.FindObjectOfType<ExplosionForce>();
        ef.doExplosion(transform.position);
    }
}