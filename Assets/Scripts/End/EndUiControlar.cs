﻿using UnityEngine;
using UnityEngine.UI;

public class EndUiControlar : MonoBehaviour
{
    public Text Winner;

    // Use this for initialization
    private void Start()
    {
        GameMgr gameMgr = GameMgr.Instance;
        var color = Winner.color;
        switch (gameMgr.m_enWinner)
        {
            case WinColor.Green:
                Winner.text = "グリーンの勝ち";
                color = new Color(0.0f, 255.0f, 0.0f);
                break;

            case WinColor.Blue:
                Winner.text = "ブルーの勝ち";
                color = new Color(0.0f, 0.0f, 255.0f);
                break;

            case WinColor.Stailment:
                Winner.text = "引き分け";
                color = new Color(255.0f, 0.0f, 0.0f);
                break;

            default:
                Winner.text = "引き分け";
                color = new Color(255.0f, 0.0f, 0.0f);
                break;
        }
        Winner.color = color;
    }

    // Update is called once per frame
    private void Update()
    {
    }
}