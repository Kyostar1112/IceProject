﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class End : MonoBehaviour
{
    private StTitleCrtl m_enPController;
    public GameObject m_Green;
    public GameObject m_Blue;

    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        m_enPController = GetComponent<EndCotroal>().m_enPController;
        GameMgr gameMgr = GameMgr.Instance;
        if (gameMgr.m_fFadeRange == 0.0f)
        {
            switch (gameMgr.m_enWinner)
            {
                case WinColor.Green:
                    break;

                case WinColor.Blue:
                    break;

                case WinColor.Stailment:
                    break;

                default:
                    break;
            }
        }
        if (m_enPController.Enter &&
            gameMgr.m_fFadeRange == 0.0f)
        {
            gameMgr.FadeOut(() =>
            {
                SceneManager.LoadSceneAsync("Title");
            });
        }
    }
}