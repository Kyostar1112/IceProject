﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiControlar : MonoBehaviour
{
    public Text m_tHp;
    public Text m_tPlayerState;
    public Text m_tPlayerChageCnt;
    public GameObject m_HpImage;
    public GameObject m_SnowBall;
    public GameObject m_gPlayer;
    private int m_iHp;
    private int m_iOldHp;
    public List<GameObject> m_ObjHeart;
    public const int NUM_MAX = 6;
    private float X_INTERVAL = 30.0f;
    private float Y_INTERVAL = 25.0f;
    private float X_FIRSTPOINT_ADD = 0.0f;

    // Use this for initialization
    private void Start()
    {
        m_iHp = m_gPlayer.GetComponent<PlayerManager>().m_iHp;
        X_INTERVAL = Screen.width / 20.0f;
        Y_INTERVAL = Screen.height / 10.0f;
        X_FIRSTPOINT_ADD = Screen.width / 30.0f;
    }

    // Update is called once per frame
    private void Update()
    {
        HpDisp();
        StateDisp();
        ChageCntDisp();
    }

    private void HpDisp()
    {
        m_iHp = m_gPlayer.GetComponent<PlayerManager>().m_iHp;
        if (m_iOldHp != m_iHp)
        {
            foreach (GameObject Heart in m_ObjHeart)
            {
                // 消す！
                Destroy(Heart);
            }
            m_ObjHeart.Clear();
            for (int i = 0; i < m_iHp; i++)
            {
                m_ObjHeart.Add(Instantiate(m_HpImage));
                m_ObjHeart[i].transform.SetParent(transform, false);

                Vector3 vScale = new Vector3(0.25f / 2, 0.25f / 2, 0.25f / 2);
                m_ObjHeart[i].transform.localScale = vScale;

                Vector3 vPosision = m_tHp.transform.position;
                vPosision.x += X_FIRSTPOINT_ADD;
                if (NUM_MAX / 2 <= i)
                {
                    vPosision.x += m_tHp.transform.localScale.x + X_INTERVAL * (i - NUM_MAX / 2);
                    vPosision.y += m_tHp.transform.localScale.y - Y_INTERVAL;
                }
                else
                {
                    vPosision.x += m_tHp.transform.localScale.x + X_INTERVAL * i;
                }
                m_ObjHeart[i].transform.position = vPosision;
            }
            m_iOldHp = m_iHp;
        }

        //m_tHp.text += m_iHp;
    }

    private void StateDisp()
    {
        switch (m_gPlayer.GetComponent<PlayerManager>().m_enPlayerSte)
        {
            case EnPlayerState.Idle:
                m_tPlayerState.text = "待機";
                break;

            case EnPlayerState.Move:
                m_tPlayerState.text = "移動";
                break;

            case EnPlayerState.Chage:
                m_tPlayerState.text = "チャージ";
                break;

            case EnPlayerState.Attack:
                m_tPlayerState.text = "攻撃";
                break;

            case EnPlayerState.AttackIdle:
                m_tPlayerState.text = "攻撃待機";
                break;

            case EnPlayerState.Squatting:
                m_tPlayerState.text = "しゃがみ";
                break;

            case EnPlayerState.Down:
                m_tPlayerState.text = "撃破";
                break;
        }
    }

    private void ChageCntDisp()
    {
        switch (m_gPlayer.GetComponent<PlayerAttack>().m_iChageCnt)
        {
            case 0:
                m_SnowBall.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
                break;

            case 1:
                m_SnowBall.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
                break;

            case 2:
                m_SnowBall.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                break;

            case 3:
                m_SnowBall.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                break;

            default:
                break;
        }
    }
}