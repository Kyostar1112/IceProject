﻿using UnityEngine;
using UnityEngine.SceneManagement;

public enum EnPlayerNum
{
    P1 = 1,
    P2 = -1,
    P3 = 0,
}

public enum EnPlayerState
{
    Idle = 0,
    Move,
    Chage,
    Squatting,
    Attack,
    AttackIdle,
    Down,
}

public enum EnLinePos
{
    Right = 0,
    Left,
    Center,
}

public class PlayerManager : MonoBehaviour
{
    public int m_iMaxChageCnt;
    public int m_iHpMax;

    public int m_iMoveIntervalMax;
    public int m_iAttakIntervalMax;

    public EnPlayerNum m_enPlayerNum;

    internal int m_iHp;

    internal int m_iMoveInterval;
    internal int m_iChargeInterval;
    internal int m_iAttakInterval;

    public EnPlayerState m_enPlayerSte;
    private EnPlayerState m_enOldPlayerSte;

    private float timeOut;
    private float timeElapsed;

    private bool m_bFadeStart;

    private void Start()
    {
        m_enPlayerSte = EnPlayerState.Idle;
        timeOut = 1;
        m_iHp = m_iHpMax;
        GameMgr.Instance.m_enWinner = WinColor.Stailment;
        m_bFadeStart = false;
    }

    private void Update()
    {
        if (m_bFadeStart)
        {
            return;
        }
        PlayerStateInit();
        if (m_iHp < 1)
        {
            WinColor m_enWinner = GameMgr.Instance.m_enWinner;
            switch (m_enPlayerNum)
            {
                case EnPlayerNum.P1:
                    m_enWinner = WinColor.Blue;
                    break;

                case EnPlayerNum.P2:
                    m_enWinner = WinColor.Green;
                    break;

                case EnPlayerNum.P3:
                    m_enWinner = WinColor.Stailment;
                    break;

                default:
                    m_enWinner = WinColor.Stailment;
                    break;
            }
            GameMgr.Instance.m_enWinner = m_enWinner;
            GameMgr gameMgr = GameMgr.Instance;

            gameMgr.FadeOut(() =>
            {
                SceneManager.LoadSceneAsync("End");
            });
            m_bFadeStart = true;
        }
    }

    private void PlayerStateInit()
    {
        if (m_enOldPlayerSte == m_enPlayerSte)
        {
            return;
        }

        switch (m_enPlayerSte)
        {
            case EnPlayerState.Move:
                if (m_iMoveInterval <= 0)
                {
                    m_enPlayerSte = EnPlayerState.Idle;
                    m_enOldPlayerSte = m_enPlayerSte;
                }
                break;

            case EnPlayerState.Chage:
                if (m_iChargeInterval <= 0)
                {
                    m_enPlayerSte = EnPlayerState.Idle;
                    m_enOldPlayerSte = m_enPlayerSte;
                }
                break;

            case EnPlayerState.Attack:
                if (m_iAttakInterval <= 0)
                {
                    m_enPlayerSte = EnPlayerState.Idle;
                    m_enOldPlayerSte = m_enPlayerSte;
                }
                break;
        }
        TimeCnt();
    }

    //時間ごとの処理.
    private void TimeCnt()
    {
        timeElapsed += Time.deltaTime;

        if (timeElapsed >= timeOut / 3)
        {
            // Do anything
            switch (m_enPlayerSte)
            {
                case EnPlayerState.Move:
                    m_iMoveInterval--;
                    break;

                case EnPlayerState.Chage:
                    m_iChargeInterval--;
                    break;

                case EnPlayerState.Attack:
                    m_iAttakInterval--;
                    break;
            }
            timeElapsed = 0.0f;
        }
    }
}