﻿using UnityEngine;

public class PlayerReceivedAttack : MonoBehaviour
{
    public GameObject Player;
    public AudioClip m_DamageSeMini;
    public AudioClip m_DamageSeNormal;
    public AudioClip m_DamageSeBig;

    private AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            var enPlayerNum = GetComponent<PlayerManager>().m_enPlayerNum;
            var enShotNum = other.GetComponent<PlayerShot>().m_enPlayerNum;
            if (enShotNum == EnPlayerNum.P3)
            {
                return;
            }
            if (enPlayerNum != enShotNum)
            {
                int Damage = (int)(other.transform.localScale.x * 2);
                switch (Damage)
                {
                    case 1:
                        Damage = 1;
                        audioSource.PlayOneShot(m_DamageSeMini);
                        break;

                    case 2:
                        Damage = 2;
                        audioSource.PlayOneShot(m_DamageSeNormal);
                        break;

                    case 3:
                        Damage = 3;
                        audioSource.PlayOneShot(m_DamageSeBig);
                        break;

                    default:
                        Damage = 0;
                        break;
                }
                GetComponent<PlayerManager>().m_iHp -= Damage;
                GetComponent<PlayerController>().SetRumble();
                int iHp = GetComponent<PlayerManager>().m_iHp;

                Destroy(other.gameObject);
                if (iHp <= 1)
                {
                    GetComponent<PlayerManager>().m_enPlayerSte = EnPlayerState.Down;
                }
            }
        }
    }
}