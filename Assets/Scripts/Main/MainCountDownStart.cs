﻿using UnityEngine;

public class MainCountDownStart : MonoBehaviour
{
    // Update is called once per frame
    private void Update()
    {
        GameMgr gameMgr = GameMgr.Instance;
        if (GetComponent<TimeUpCount>().totalTime < 1)
        {
            Destroy(this.gameObject);
        }
        if (gameMgr.m_fFadeRange == 0.0f)
        {
            GetComponent<TimeUpCount>().m_bCountStart = true;
        }
    }
}