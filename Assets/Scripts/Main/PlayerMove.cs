﻿using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    //アニメーション.
    public AnimationClip m_MoveAnimation;

    //アニメーション.
    public float m_fMoveAnimationTime;

    //アニメーションコントローラー.
    private Animator m_AnimCtrl;

    //ゲームコントローラー.
    private StController m_Controller;

    //プレイヤーの番号.
    private EnPlayerNum m_PlayerNum;

    //プレイヤーの位置.
    internal EnLinePos m_enPlayeLinePos;

    //プレイヤーの行動状態.
    internal EnPlayerState m_enPlayerSte;

    //移動の行動間隔.
    private int m_iMoveIntervalMax;

    //移動時の一回だけ通る処理.
    bool m_bMoveInitFlg;
   
    //前フレームでの位置.
    EnLinePos m_enOldLinePos;

    // Use this for initialization
    private void Start()
    {
        m_bMoveInitFlg = false;
        m_AnimCtrl = GetComponent<Animator>();
        m_PlayerNum = GetComponent<PlayerManager>().m_enPlayerNum;
        m_iMoveIntervalMax = GetComponent<PlayerManager>().m_iMoveIntervalMax;
        m_enPlayeLinePos = EnLinePos.Center;
    }

    // Update is called once per frame
    private void Update()
    {
        m_fMoveAnimationTime = m_MoveAnimation.length;
        m_enPlayerSte = GetComponent<PlayerManager>().m_enPlayerSte;
        m_Controller = GetComponent<PlayerController>().m_enPController;
        PlayerControal();
        KeyControal();
        GetComponent<PlayerManager>().m_enPlayerSte = m_enPlayerSte;
    }

    private void PlayerControal()
    {
        Vector3 pos;
        //プレイヤーごとに処理を分ける.
        switch (m_PlayerNum)
        {
            case EnPlayerNum.P1:
                //どの位置にいればよいか.
                switch (m_enPlayeLinePos)
                {
                    case EnLinePos.Right:
                        pos = gameObject.transform.position;
                        gameObject.transform.position = new Vector3(pos.x = 1.0f, pos.y, pos.z);
                        break;

                    case EnLinePos.Left:
                        pos = gameObject.transform.position;
                        gameObject.transform.position = new Vector3(pos.x = -1.0f, pos.y, pos.z);
                        break;

                    case EnLinePos.Center:
                        pos = gameObject.transform.position;
                        gameObject.transform.position = new Vector3(pos.x = 0.0f, pos.y, pos.z);
                        break;
                }
                break;

            case EnPlayerNum.P2:
                //どの位置にいればよいか.
                switch (m_enPlayeLinePos)
                {
                    case EnLinePos.Right:
                        pos = gameObject.transform.position;
                        gameObject.transform.position = new Vector3(pos.x = -1.0f, pos.y, pos.z);
                        break;

                    case EnLinePos.Left:
                        pos = gameObject.transform.position;
                        gameObject.transform.position = new Vector3(pos.x = 1.0f, pos.y, pos.z);
                        break;

                    case EnLinePos.Center:
                        pos = gameObject.transform.position;
                        gameObject.transform.position = new Vector3(pos.x = 0.0f, pos.y, pos.z);
                        break;
                }
                break;
        }
    }

    private void KeyControal()
    {
        //下キー操作.
        if (m_Controller.bSquattingFlg)
        {
            m_AnimCtrl.SetBool("SquatDown", true);
            m_enPlayerSte = EnPlayerState.Squatting;
            GetComponent<PlayerManager>().m_iMoveInterval = m_iMoveIntervalMax;
        }
        else
        {
            m_AnimCtrl.SetBool("SquatDown", false);
            m_enPlayerSte = EnPlayerState.Idle;
            GetComponent<PlayerManager>().m_iMoveInterval = m_iMoveIntervalMax;
        }

        bool bAttackDelayFlg = GetComponent<PlayerController>().m_enPController.bAttackDelayFlg;
        var animator = GetComponent<Animator>();
        //引数0はLayerのdefault。
        //上から順番の0,1,2,3.....のように割り当てられます。
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
        int CurrentAnimation = stateInfo.nameHash;
        int IdleAnimation = Animator.StringToHash("Base Layer.Chara1_Idle");
        int AttackIdleAnimation = Animator.StringToHash("Base Layer.chara1_ThrowBallIdle");
        int MoveAnimation = Animator.StringToHash("Base Layer.chara1_Step");

        if (CurrentAnimation == IdleAnimation ||
            CurrentAnimation == AttackIdleAnimation ||
            CurrentAnimation == MoveAnimation)
        {
            if (bAttackDelayFlg)
            {
                m_enPlayerSte = EnPlayerState.AttackIdle;
            }
        }

        if (m_enPlayerSte == EnPlayerState.Idle &&
            CurrentAnimation == IdleAnimation)
        {
            if (m_enOldLinePos != m_enPlayeLinePos)
            {
                m_AnimCtrl.SetTrigger("Step");
                m_enOldLinePos = m_enPlayeLinePos;
            }
            //右キー操作.
            if (m_Controller.fLh == 1)
            {
                m_enPlayeLinePos = EnLinePos.Right;
                m_enPlayerSte = EnPlayerState.Move;
                GetComponent<PlayerManager>().m_iMoveInterval = m_iMoveIntervalMax;
            }
            else//左キー操作.
            if (m_Controller.fLh == -1)
            {
                m_enPlayeLinePos = EnLinePos.Left;
                m_enPlayerSte = EnPlayerState.Move;
                GetComponent<PlayerManager>().m_iMoveInterval = m_iMoveIntervalMax;
            }
            else if (m_Controller.fLh == 0.0f)//元の位置に.
            {
                m_enPlayeLinePos = EnLinePos.Center;
            }
        }
    }
}