﻿using UnityEngine;

public enum ItemState
{
    Idle,
    Move,
}

public class Item : MonoBehaviour
{
    private enum ItemMode
    {
        Right,
        Left,
    }

    private ItemMode m_ItemMode;

    public ItemState m_itemState;

    public float m_MoveSpeed;

    // Use this for initialization
    private void Start()
    {
        m_ItemMode = ItemMode.Right;
    }

    // Update is called once per frame
    private void Update()
    {
        if (m_itemState == ItemState.Idle)
        {
            Vector3 Tmp = this.transform.position;
            Tmp.x = 5.0f;
            this.transform.position = Tmp;
            return;
        }
        this.transform.Rotate(0, 30, 0);
        Vector3 Pos = this.transform.position;
        switch (m_ItemMode)
        {
            case ItemMode.Right:
                Pos.x += m_MoveSpeed;
                if (Pos.x > 5.0f)
                {
                    m_ItemMode = ItemMode.Left;
                }
                break;

            case ItemMode.Left:
                Pos.x -= m_MoveSpeed;
                if (Pos.x < -5.0f)
                {
                    m_ItemMode = ItemMode.Right;
                }
                break;

            default:
                break;
        }
        this.transform.position = Pos;
    }
}