﻿using UnityEngine;

public class MainTimerStart : MonoBehaviour
{
    private GameObject Obj;

    // Use this for initialization
    private void Start()
    {
        Obj = GameObject.FindGameObjectWithTag("CountDown");
    }

    // Update is called once per frame
    private void Update()
    {
        if (!Obj)
        {
            return;
        }
        if (Obj.GetComponent<TimeUpCount>().totalTime > 1)
        {
            return;
        }
        GetComponent<TimeUpCount>().m_bCountStart = true;
    }
}