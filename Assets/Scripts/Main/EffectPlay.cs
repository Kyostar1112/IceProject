﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EffectPlay : MonoBehaviour
{
    private List<GameObject> m_Obj;
    private bool m_bStartFlg;

    // Use this for initialization
    private void Start()
    {
        m_Obj = new List<GameObject>();
        m_bStartFlg = false;
        var TmpObj = GetChildren();
        for (int i = 0; i < TmpObj.Length; i++)
        {
            m_Obj.Add(TmpObj[i]);
        }
        m_Obj.RemoveAt(0);
        m_Obj.Count();
    }

    // Update is called once per frame
    private void Update()
    {
        if (m_bStartFlg)
        {
            foreach (var Obj in m_Obj)
            {
                if (Obj.GetComponent<ParticleSystem>().IsAlive())
                {
                    return;
                }
            }
            foreach (var Obj in m_Obj)
            {
                Destroy(Obj);
            }
            Destroy(this.gameObject);
            return;
        }

        foreach (var item in m_Obj)
        {
            var TmpParticle = item.GetComponent<ParticleSystem>();
            var main = TmpParticle.main;
            //Destroy(gameObject, main.duration);
            TmpParticle.Play();
            m_bStartFlg = true;
        }
    }

    private GameObject[] GetChildren(string parentName)
    {
        // 検索し、GameObject型に変換
        var parent = GameObject.Find(parentName) as GameObject;
        // 見つからなかったらreturn
        if (parent == null) return null;
        // 子のTransform[]を取り出す
        var transforms = parent.GetComponentsInChildren<Transform>();
        // 使いやすいようにtransformsからgameObjectを取り出す
        var gameObjects = from t in transforms select t.gameObject;
        // 配列に変換してreturn
        return gameObjects.ToArray();
    }

    private GameObject[] GetChildren()
    {
        // 検索し、GameObject型に変換
        var parent = this;
        // 見つからなかったらreturn
        if (parent == null) return null;
        // 子のTransform[]を取り出す
        var transforms = parent.GetComponentsInChildren<Transform>();
        // 使いやすいようにtransformsからgameObjectを取り出す
        var gameObjects = from t in transforms select t.gameObject;
        // 配列に変換してreturn
        return gameObjects.ToArray();
    }
}