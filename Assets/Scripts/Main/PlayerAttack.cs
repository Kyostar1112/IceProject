﻿using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    //雪玉のチャージ段階.
    internal int m_iChageCnt;

    //アニメーションコントローラー.
    private Animator m_AnimCtrl;

    //攻撃フラグ.
    private bool m_bAttackFlg;

    //攻撃のできる間隔.
    private int m_iAttakIntervalMax;

    //プレイヤー番号.
    private EnPlayerNum m_PlayerNum;

    //プレイヤーの3ラインの位置.
    private EnLinePos m_enPlayeLinePos;

    //プレイヤーの行動状態.
    private EnPlayerState m_enPlayerSte;

    //雪玉ゲームオブジェクト.
    public GameObject m_gBullet;

    //最大のチャージ段階.
    public float m_iChageMax = 3;

    //内部で使うチャージカウント用.
    public int m_iChagePoint;

    //雪玉の投げる位置.
    public GameObject m_gTarget;

    public GameObject m_ball;

    public GameObject SnowParticle;

    private string m_SnowEffectTagStr;

    public GameObject m_Rhand;

    private int  m_iAttackWait;
    private bool m_bAttackWaitFlg;

    private AudioSource m_ThrowSe;
    private AudioSource m_AttackIdleSe;

    // Use this for initialization
    private void Start()
    {
        AudioSource[] audiosource = GetComponents<AudioSource>();
        m_ThrowSe = audiosource[0];
        m_AttackIdleSe = audiosource[1];
        m_AnimCtrl = GetComponent<Animator>();
        m_PlayerNum = GetComponent<PlayerManager>().m_enPlayerNum;
        m_iAttakIntervalMax = GetComponent<PlayerManager>().m_iAttakIntervalMax;
        m_bAttackWaitFlg = false;
        switch (m_PlayerNum)
        {
            case EnPlayerNum.P1:
                m_SnowEffectTagStr = "SnowEffectP1";
                break;

            case EnPlayerNum.P2:
                m_SnowEffectTagStr = "SnowEffectP2";
                break;
        }
    }

    // Update is called once per frame
    private void Update()
    {
        m_enPlayerSte = GetComponent<PlayerManager>().m_enPlayerSte;
        m_enPlayeLinePos = GetComponent<PlayerMove>().m_enPlayeLinePos;

        m_bAttackFlg = GetComponent<PlayerController>().m_enPController.bAttackFlg;

        switch (m_enPlayerSte)
        {
            //しゃがみ状態.
            case EnPlayerState.Squatting:
                if (GetComponent<PlayerController>().m_enPController.bChageFlg)
                {
                    m_enPlayerSte = EnPlayerState.Chage;
                }
                break;

            case EnPlayerState.Down:
                break;

            default:
                break;
        }
        if (m_enPlayerSte == EnPlayerState.Chage)
        {
            AttackCharge();
        }
        else
        {
            m_AnimCtrl.SetBool("SnowCharge", false);
        }
        if (m_enPlayerSte == EnPlayerState.AttackIdle)
        {
            if (m_ball != null)
            {
                if (m_bAttackFlg)
                {
                    m_enPlayerSte = EnPlayerState.Attack;
                }
            }
            else
            {
                if (m_iChageCnt > 0)
                {
                    AttakIdle();
                }
            }
        }
        else
        {
            m_AnimCtrl.SetBool("BallIdle", false);
        }

        if (m_enPlayerSte == EnPlayerState.Attack)
        {
            if (m_ball != null)
            {
                AttakInit();
            }
        }

        if (m_ball != null)
        {

            //引数0はLayerのdefault。
            //上から順番の0,1,2,3.....のように割り当てられます。
            AnimatorStateInfo stateInfo = m_AnimCtrl.GetCurrentAnimatorStateInfo(0);
            int CurrentAnimation = stateInfo.nameHash;
            int ThrowBallIdleAnimation = Animator.StringToHash("Base Layer.chara1_ThrowBallIdle");
            int ThrowBallAnimation = Animator.StringToHash("Base Layer.chara1_ThrowBall");
            if (CurrentAnimation != ThrowBallIdleAnimation &&
                CurrentAnimation != ThrowBallAnimation)
            {
                Destroy(m_ball);
            }
        }
        Attack();
        GetComponent<PlayerManager>().m_enPlayerSte = m_enPlayerSte;
    }

    private void AttackCharge()
    {
        //チャージアニメーション表示.
        if (m_iChageCnt < m_iChageMax)
        {
            var pos = transform.position;
            pos.y -= 0.5f;
            SnowParticle.transform.position = pos;
            var Obj = Instantiate(SnowParticle);
            Obj.tag = m_SnowEffectTagStr;
            m_AnimCtrl.SetBool("SnowCharge", true);
            ++m_iChagePoint;
            switch (m_iChagePoint)
            {
                case 20:
                    ++m_iChageCnt;
                    break;
                case 40:
                    ++m_iChageCnt;
                    break;
                case 60:
                    ++m_iChageCnt;
                    break;
                default:
                    break;
            }
        }
    }

    private void AttakIdle()
    {
        var vPosision = this.transform.position;
        m_AttackIdleSe.PlayOneShot(m_AttackIdleSe.clip);
        // Ballオブジェクトの生成
        m_AnimCtrl.SetBool("BallIdle", true);
        m_ball = Instantiate(m_gBullet);
        m_ball.GetComponent<PlayerShot>().ShotInit(m_PlayerNum, m_Rhand, m_iChageCnt);
        switch (GetComponent<PlayerManager>().m_enPlayerNum)
        {
            case EnPlayerNum.P1:
                m_ball.name = "P1Ball";
                break;
            case EnPlayerNum.P2:
                m_ball.name = "P2Ball";
                break;
            default:
                break;
        }
    }

    private void AttakInit()
    {
        if (m_bAttackWaitFlg)
        {
            return;
        }
        //引数0はLayerのdefault。
        //上から順番の0,1,2,3.....のように割り当てられます。
        AnimatorStateInfo stateInfo = m_AnimCtrl.GetCurrentAnimatorStateInfo(0);
        int CurrentAnimation = stateInfo.nameHash;
        int IdleAnimation = Animator.StringToHash("Base Layer.chara1_ThrowBallIdle");
        if (CurrentAnimation != IdleAnimation)
        {
            return;
        }
        m_enPlayerSte = EnPlayerState.Attack;
        m_AnimCtrl.SetTrigger("Attack");
        const int AttackAnmationFrame = 15;
        m_iAttackWait = AttackAnmationFrame;
        m_iChagePoint = 0;
        //攻撃開始.
        m_ball.GetComponent<PlayerShot>().m_enPlayerNum = m_PlayerNum;
        m_bAttackFlg = false;
        m_iChageCnt = 0;
        m_bAttackWaitFlg = true;
    }

    private void Attack()
    {
        if (!m_bAttackWaitFlg)
        {
            return;
        }
        if (m_iAttackWait>1)
        {
            --m_iAttackWait;
        }
        else
        {
            if (m_ball == null)
            {
                return;
            }
            m_ThrowSe.PlayOneShot(m_ThrowSe.clip);
            GetComponent<ThrowingScript>().ThrowingBall(m_gTarget, m_ball);
            m_ball.GetComponent<PlayerShot>().m_bAttackIdle = true;
            m_bAttackWaitFlg = false;
            GetComponent<PlayerManager>().m_iAttakInterval = m_iAttakIntervalMax;
            m_ball = null;
        }
    }
}