﻿using UnityEngine;

public struct StShotParamator
{
    public bool bShotFlg;
    public Vector3 vShotPos;
    public EnLinePos ShotLine;
}

public class PlayerShot : MonoBehaviour
{
    public EnPlayerNum m_enPlayerNum;
    public GameObject SnowParticle;
    private GameObject m_RHand;
    public bool m_bAttackIdle;
    // Use this for initialization
    private void Awake()
    {
        m_bAttackIdle = false;
        m_enPlayerNum = EnPlayerNum.P3;
    }

    // Update is called once per frame
    private void Update()
    {
        //Vector3 tmp = transform.position;
        //switch (m_enPlayerNum)
        //{
        //    case EnPlayerNum.P1:
        //        tmp.z += 0.1f;
        //        break;

        //    case EnPlayerNum.P2:
        //        tmp.z -= 0.1f;
        //        break;
        //}
        //transform.position = tmp;

        //if (Mathf.Abs(transform.position.z) > 10)
        //{
        //    Destroy(this.gameObject);
        //}
        if (!m_bAttackIdle)
        {
            var tmp = m_RHand.transform.position;
            tmp.y += transform.localScale.y/2;
            transform.position = tmp;
        }
    }

    public void ShotInit(EnPlayerNum id, GameObject RHand, float Scale)
    {
        m_enPlayerNum = id;
        m_RHand = RHand;
#if false
    switch (m_enPlayerNum)
    {
        case EnPlayerNum.P1:
            //tmp.z = -8;
            switch (m_ShotLine)
            {
                case EnLinePos.Right:
                    tmp.x = 1;
                    break;

                case EnLinePos.Left:
                    tmp.x = -1;
                    break;

                case EnLinePos.Center:
                    tmp.x = 0;
                    break;
            }
            break;

        case EnPlayerNum.P2:
            //tmp.z = 8;
            switch (m_ShotLine)
            {
                case EnLinePos.Right:
                    tmp.x = -1;
                    break;

                case EnLinePos.Left:
                    tmp.x = 1;
                    break;

                case EnLinePos.Center:
                    tmp.x = 0;
                    break;
            }
            break;
    }
#endif
        Vector3 vScale;
        float fScale = Scale;
        vScale.x = fScale / 2;
        vScale.y = fScale / 2;
        vScale.z = fScale / 2;
        transform.localScale = vScale;

        var tmp = m_RHand.transform.position;
        tmp.y += transform.localScale.y/2;
        transform.position = tmp;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            if (other.transform.localScale.x == transform.localScale.x)
            {
                SnowParticle.transform.position = transform.position;
                var Obj = Instantiate(SnowParticle);
                Destroy(gameObject);
                Destroy(other.gameObject);
            }
            if (other.transform.localScale.x > transform.localScale.x)
            {
                Destroy(this.gameObject);
            }
            else
            {
                Destroy(other.gameObject);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Stage")
        {
            Destroy(gameObject);
        }
    }
}