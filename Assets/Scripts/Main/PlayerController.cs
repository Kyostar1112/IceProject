﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public struct StController
{
    public float fRh;        //右コントローラースティックの値.
    public float fRv;        //右コントローラースティックの値.
    public float fLh;        //左コントローラースティックの値.
    public float fLv;        //左コントローラースティックの値.
    public Vector3 vRAccelPoint;//右加速値.
    public Vector3 vLAccelPoint;//左加速値.
    public Quaternion vRVectorPoint;//右傾きの値.
    public Quaternion vLVectorPoint;//左傾きの値.
    public bool bAttackFlg;         //攻撃フラグ.
    public bool bAttackDelayFlg;    //攻撃構えフラグ.
    public bool bSquattingFlg;      //しゃがみフラグ.
    public bool bChageFlg;          //攻撃チャージフラグ.
}

public enum JoyConState
{
    Start = 0,
    Halfway,
    End,
}

public class PlayerController : MonoBehaviour
{
    public StController m_enPController;
    private EnPlayerNum m_enPlayerNum;

    public bool m_bJoyCon = false;

    //JoyCon追加.
    public GameObject Joycon;

    private static readonly Joycon.Button[] m_buttons
        = Enum.GetValues(typeof(Joycon.Button)) as Joycon.Button[];

    private List<Joycon> m_joycons;
    private const int P1LJoyConNum = 0;
    private const int P1RJoyConNum = 1;
    private const int P2LJoyConNum = 2;
    private const int P2RJoyConNum = 3;
    private const int ZrJoyConButtonNum = 12;
    private const int RJoyConButtonNum = 11;
    public Vector4 Vivration = new Vector4(160, 320, 0.6f, 200);
    private Joycon.Button? m_pressedButtonL;
    private Joycon.Button? m_pressedButtonR;
    private Joycon.Button? m_pressedButton2L;
    private Joycon.Button? m_pressedButton2R;

    private GameObject m_CountDown;

    private JoyConState m_enChageJoyConState;

    public float fLh;
    public float fLv;

    private void Start()
    {
        m_CountDown = GameObject.FindGameObjectWithTag("CountDown");
        m_joycons = GameMgr.Instance.JoyCon.j;
        if (m_joycons == null || m_joycons.Count <= 0) return;
        m_bJoyCon = true;
        m_enChageJoyConState = JoyConState.Start;
    }

    // Update is called once per frame
    private void Update()
    {
        GameMgr gameMgr = GameMgr.Instance;
        if (gameMgr.m_fFadeRange != 0.0f)
        {
            return;
        }
        //ｷｰ入力.
        m_enPlayerNum = GetComponent<PlayerManager>().m_enPlayerNum;
        if (!m_CountDown)
        {
            if (m_bJoyCon)
            {
                JoyCon();
            }
            else
            {
                ConNormal();
            }
        }

        Axiz();

        if (Input.GetButtonDown("Enter") &&
            gameMgr.m_fFadeRange == 0.0f)
        {
            gameMgr.FadeOut(() =>
            {
                SceneManager.LoadSceneAsync("End");
            });
        }

        fLh = m_enPController.fLh;
        fLv = m_enPController.fLv;
    }

    private void ConNormal()
    {
        switch (m_enPlayerNum)
        {
            case EnPlayerNum.P1:
                //ｷｰ入力.
                m_enPController.fLh = Input.GetAxis("P1HBehavior");
                m_enPController.fLv = Input.GetAxis("P1VBehavior");
                if (Input.GetButton("P1Shot"))
                {
                    m_enPController.bAttackFlg = true;
                }
                else
                {
                    m_enPController.bAttackFlg = false;
                }

                if (Input.GetButton("P1ShotDelay"))
                {
                    m_enPController.bAttackDelayFlg = true;
                }
                else
                {
                    m_enPController.bAttackDelayFlg = false;
                }

                if (Input.GetButtonDown("P1Chage"))
                {
                    m_enPController.bChageFlg = true;
                }
                else
                {
                    m_enPController.bChageFlg = false;
                }

                if (Input.GetButton("P1Squatting"))
                {
                    m_enPController.bSquattingFlg = true;
                }
                else
                {
                    m_enPController.bSquattingFlg = false;
                }

                break;

            case EnPlayerNum.P2:
                if (Input.GetButton("P2Shot"))
                {
                    m_enPController.bAttackFlg = true;
                }
                else
                {
                    m_enPController.bAttackFlg = false;
                }

                if (Input.GetButton("P2ShotDelay"))
                {
                    m_enPController.bAttackDelayFlg = true;
                }
                else
                {
                    m_enPController.bAttackDelayFlg = false;
                }

                if (Input.GetButtonDown("P2Chage"))
                {
                    m_enPController.bChageFlg = true;
                }
                else
                {
                    m_enPController.bChageFlg = false;
                }

                if (Input.GetButton("P2Squatting"))
                {
                    m_enPController.bSquattingFlg = true;
                }
                else
                {
                    m_enPController.bSquattingFlg = false;
                }
                break;
        }
    }

    private void JoyCon()
    {
        Vector3 EndPos = new Vector3(0.6f, 0.9f, -0.1f);

        float fMovePointStart = 3.0f;
        float fMovePointAccel;

        switch (m_enPlayerNum)
        {
            case EnPlayerNum.P1:
                //加速度・傾き入力受け取る.
                m_enPController.vRAccelPoint = m_joycons[P1RJoyConNum].GetAccel();
                m_enPController.vRVectorPoint = m_joycons[P1RJoyConNum].GetVector();

                m_enPController.vLAccelPoint = m_joycons[P1LJoyConNum].GetAccel();
                m_enPController.vLVectorPoint = m_joycons[P1LJoyConNum].GetVector();

                if (m_joycons[P1RJoyConNum].GetButton(m_buttons[ZrJoyConButtonNum]))
                {
                    m_enPController.bSquattingFlg = true;
                }
                else
                {
                    m_enPController.bSquattingFlg = false;
                }

                if (m_joycons[P1RJoyConNum].GetButton(m_buttons[RJoyConButtonNum]))
                {
                    m_enPController.bAttackDelayFlg = true;
                }
                else
                {
                    m_enPController.bAttackDelayFlg = false;
                }
                break;

            case EnPlayerNum.P2:
                if (m_joycons == null || m_joycons.Count <= 2) return;

                //加速度・傾き入力受け取る.
                m_enPController.vRAccelPoint = m_joycons[P2RJoyConNum].GetAccel();
                m_enPController.vLAccelPoint = m_joycons[P2LJoyConNum].GetAccel();
                m_enPController.vRVectorPoint = m_joycons[P2RJoyConNum].GetVector();
                m_enPController.vLVectorPoint = m_joycons[P2LJoyConNum].GetVector();

                if (m_joycons[P2RJoyConNum].GetButton(m_buttons[ZrJoyConButtonNum]))
                {
                    m_enPController.bSquattingFlg = true;
                }
                else
                {
                    m_enPController.bSquattingFlg = false;
                }

                if (m_joycons[P2RJoyConNum].GetButton(m_buttons[RJoyConButtonNum]))
                {
                    m_enPController.bAttackDelayFlg = true;
                }
                else
                {
                    m_enPController.bAttackDelayFlg = false;
                }
                break;
        }

        fMovePointAccel =
             Mathf.Abs(m_enPController.vRAccelPoint.x) +
             Mathf.Abs(m_enPController.vRAccelPoint.y) +
             Mathf.Abs(m_enPController.vRAccelPoint.z);

        m_enPController.bChageFlg = false;

        switch (m_enChageJoyConState)
        {
            case JoyConState.Start:
                if (m_enPController.bSquattingFlg)
                {
                    m_enChageJoyConState = JoyConState.Halfway;
                }
                break;

            case JoyConState.Halfway:
                if (fMovePointStart < fMovePointAccel)
                {
                    m_enChageJoyConState = JoyConState.End;
                }
                break;

            case JoyConState.End:
                m_enPController.bChageFlg = true;
                m_enChageJoyConState = JoyConState.Start;
                break;

            default:
                break;
        }

        m_enPController.bAttackFlg = false;

        switch (m_enChageJoyConState)
        {
            case JoyConState.Start:
                if (m_enPController.bAttackDelayFlg)
                {
                    m_enChageJoyConState = JoyConState.Halfway;
                }
                break;

            case JoyConState.Halfway:
                if (fMovePointStart < fMovePointAccel)
                {
                    m_enChageJoyConState = JoyConState.End;
                }
                break;

            case JoyConState.End:
                m_enPController.bAttackFlg = true;
                m_enChageJoyConState = JoyConState.Start;
                break;

            default:
                break;
        }
    }

    private void Axiz()
    {
        switch (m_enPlayerNum)
        {
            case EnPlayerNum.P1:
                //ｷｰ入力.
                m_enPController.fLh = Input.GetAxisRaw("P1HBehavior");
                m_enPController.fLv = Input.GetAxisRaw("P1VBehavior");
                break;

            case EnPlayerNum.P2:
                //ｷｰ入力.
                m_enPController.fLh = Input.GetAxisRaw("P2HBehavior");
                m_enPController.fLv = Input.GetAxisRaw("P2VBehavior");
                break;
        }
    }

    public void SetRumble()
    {
        switch (m_enPlayerNum)
        {
            case EnPlayerNum.P1:
                if (m_joycons.Count > 0)
                {
                    m_joycons[P1RJoyConNum].SetRumble(Vivration.x, Vivration.y, Vivration.z, (int)Vivration.w);
                    m_joycons[P1LJoyConNum].SetRumble(Vivration.x, Vivration.y, Vivration.z, (int)Vivration.w);
                }
                break;

            case EnPlayerNum.P2:
                if (m_joycons.Count > 2)
                {
                    m_joycons[P2RJoyConNum].SetRumble(Vivration.x, Vivration.y, Vivration.z, (int)Vivration.w);
                    m_joycons[P2LJoyConNum].SetRumble(Vivration.x, Vivration.y, Vivration.z, (int)Vivration.w);
                }
                break;
        }
    }
}