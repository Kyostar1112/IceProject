﻿using UnityEngine;
using UnityEngine.UI;

public class TimeUpCount : MonoBehaviour
{
    //　トータル制限時間
    public float totalTime;

    //　制限時間（分）
    [SerializeField]
    private int minute;

    //　制限時間（秒）
    [SerializeField]
    private float seconds;

    //　前回Update時の秒数
    private float oldSeconds;

    //時間表示用のテキスト.
    private Text timerText;

    //制限時間の終わりを告げる.
    public bool m_bTimeOut;

    //カウントダウン開始.
    public bool m_bCountStart;

    private void Start()
    {
        m_bTimeOut = false;
        m_bCountStart = false;
        totalTime = minute * 60 + seconds;
        oldSeconds = 0.0f;
        timerText = GetComponentInChildren<Text>();
    }

    private void Update()
    {
        //　制限時間が0秒以下なら何もしない
        if (totalTime <= 0f)
        {
            return;
        }
        if (!m_bCountStart)
        {
            return;
        }
        //　一旦トータルの制限時間を計測；
        totalTime = minute * 60 + seconds;
        totalTime -= Time.deltaTime;

        //　再設定
        minute = (int)totalTime / 60;
        seconds = totalTime - minute * 60;

        //　タイマー表示用UIテキストに時間を表示する
        if ((int)seconds != (int)oldSeconds)
        {
            int Tmp = (int)totalTime;
            timerText.text = Tmp.ToString();
        }
        oldSeconds = seconds;
        //　制限時間以下になったらコンソールに『制限時間終了』という文字列を表示する
        if (totalTime <= 0.0f)
        {
            m_bTimeOut = true;
        }
    }
}