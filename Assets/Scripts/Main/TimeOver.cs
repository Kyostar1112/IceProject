﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeOver : MonoBehaviour
{
    private GameObject m_Timer;
    public GameObject m_pPlayer_g;
    public GameObject m_pPlayer_b;
    private bool m_bFadeStart;
    private AudioSource SeGong;
    // Use this for initialization
    private void Start()
    {
        SeGong = GetComponent<AudioSource>();
        m_bFadeStart = false;
        m_Timer = GameObject.FindGameObjectWithTag("Timer");
    }

    // Update is called once per frame
    private void Update()
    {
        if (!m_Timer.GetComponent<TimeUpCount>().m_bTimeOut)
        {
            return;
        }
        GameMgr gameMgr = GameMgr.Instance;
        if (gameMgr.m_fFadeRange == 0.0f &&
            !m_bFadeStart)
        {
            var P1 = m_pPlayer_g.GetComponent<PlayerManager>();
            var P2 = m_pPlayer_b.GetComponent<PlayerManager>();
            if (P1.m_iHp > P2.m_iHp)
            {
                gameMgr.m_enWinner = WinColor.Green;
            }
            else if (P1.m_iHp < P2.m_iHp)
            {
                gameMgr.m_enWinner = WinColor.Blue;
            }
            SeGong.PlayOneShot(SeGong.clip);
            gameMgr.FadeOut(() =>
            {
                SceneManager.LoadSceneAsync("End");
            });
            m_bFadeStart = true;
        }
    }
}