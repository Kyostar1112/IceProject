﻿using System;
using System.Collections.Generic;
using UnityEngine;

public struct StTitleCrtl
{
    public bool Enter;
    public float fv;
}

public class TitleCotroal : MonoBehaviour
{
    public StTitleCrtl m_enPController;

    //JoyCon追加.
    public GameObject Joycon;

    private static readonly Joycon.Button[] m_buttons
        = Enum.GetValues(typeof(Joycon.Button)) as Joycon.Button[];

    private List<Joycon> m_joycons;
    private const int P1RJoyConNum = 1;
    private const int ZrJoyConButtonNum = 12;

    private void Start()
    {
        m_joycons = GameMgr.Instance.JoyCon.j;
        if (m_joycons == null || m_joycons.Count <= 0) return;
    }

    // Update is called once per frame
    private void Update()
    {
        //ｷｰ入力.
        m_enPController.fv = Input.GetAxis("P1VBehavior");
        if (m_joycons.Count > 0)
        {
            if (Input.GetButtonDown("Enter") ||
                m_joycons[P1RJoyConNum].GetButton(m_buttons[ZrJoyConButtonNum]))
            {
                m_enPController.Enter = true;
            }
            else
            {
                m_enPController.Enter = false;
            }
        }
        else
        {
            if (Input.GetButtonDown("Enter"))
            {
                m_enPController.Enter = true;
            }
            else
            {
                m_enPController.Enter = false;
            }
        }
    }
}