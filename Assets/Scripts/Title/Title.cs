﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour
{
    private StTitleCrtl m_enPController;

    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        m_enPController = GetComponent<TitleCotroal>().m_enPController;
        GameMgr gameMgr = GameMgr.Instance;

        if (m_enPController.Enter &&
            gameMgr.m_fFadeRange == 0.0f)
        {
            gameMgr.FadeOut(() =>
            {
                SceneManager.LoadSceneAsync("Main");
            });
        }
    }
}