﻿using UnityEngine;
using UnityEngine.SceneManagement;

public enum WinColor
{
    Green = 0,
    Blue,
    Stailment
}

public enum Scene
{
    None = 0,
    Title,
    Main,
    End,
}

public class GameMgr : MonoBehaviour
{
    public static GameMgr Instance
    {
        get;
        private set;
    }

    //変数宣言.
    public WinColor m_enWinner;

    private string strScene;

    public float FadeFrameConversionQuantity;

    public Scene scene = Scene.None;
    private Scene Oldscene = Scene.None;
    public GameObject FadeCanbas;       //作るキャンバスの元.
    public JoyconManager JoyCon;
    private Fade Fade;
    public float m_fFadeRange;

    private AudioSource m_FadeSe;


    private void Awake()
    {
        m_FadeSe = GetComponent<AudioSource>();
        //Application.targetFrameRate = 60;

        //Time.captureFramerate = 60;
        if (Instance == null)
        {
            Instance = this;
            Fade = FadeCanbas.GetComponent<Fade>();
            DontDestroyOnLoad(gameObject);
            return;
        }
        Destroy(gameObject);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_STANDALONE
            Application.Quit();
#endif
        }
        strScene = SceneManager.GetActiveScene().name;
        switch (strScene)
        {
            case "Title":
                scene = Scene.Title;
                break;

            case "Main":
                scene = Scene.Main;
                break;

            case "End":
                scene = Scene.End;
                break;

            default:
                scene = Scene.None;
                break;
        }
        if (scene != Oldscene)
        {
            FadeCanbas.GetComponent<Fade>().fade.Range = 1.0f;
            FadeIn();
            Oldscene = scene;
        }
        m_fFadeRange = FadeCanbas.GetComponent<Fade>().fade.Range;
    }

    private void WinnerReset()
    {
        m_enWinner = WinColor.Stailment;
    }

    public void FadeIn(System.Action action = null)
    {
        Fade.FadeOut(FadeFrameConversionQuantity, action);
    }

    public void FadeOut(System.Action action = null)
    {
        m_FadeSe.PlayOneShot(m_FadeSe.clip);
        Fade.FadeIn(FadeFrameConversionQuantity, action);
    }
}