﻿using UnityEngine;

public class GameInitial
{
    [RuntimeInitializeOnLoadMethod]
    private static void OnRuntimeMethodLoad()
    {
        Screen.SetResolution(1600, 900, false, 60);
    }
}